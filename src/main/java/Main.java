import com.google.firebase.database.*;
import com.google.firebase.internal.NonNull;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import java.util.List;
import java.util.Scanner;

public class Main {



    public static void main(String[] args) throws IOException, URISyntaxException {

        final FireBaseService fbs = new FireBaseService();
        final DatabaseReference db = fbs.getDb().getReference("User");
        final List<User> users = new ArrayList<>();

        ValueEventListener valueEventListener = new ValueEventListener() {
            @Override
            public void onDataChange(@NonNull DataSnapshot dataSnapshot) {
                System.out.println("DataChange");

                try {
                    final User value = dataSnapshot.getValue(User.class);
                    System.out.println(value);
                    users.add(value);
                } catch (Exception e) {
                    System.out.println(e.getMessage());
                    e.printStackTrace();
                }
            }
            @Override
            public void onCancelled(@NonNull DatabaseError databaseError) {
                System.out.println("Error");
            }
        };
        db.addValueEventListener(valueEventListener);



        Scanner sc = new Scanner(System.in);
        int choice ;

        while (true){
            System.out.println("2 to view users");
            System.out.println("1 to add users");
            choice  = sc.nextInt();
            if(choice == 1){
                System.out.println("name and surname");
                String name = sc.next();
                String surname = sc.next();
                User user = new User(name,surname);
//                db.push().setValueAsync(user);
                db.child(user.getName()).setValueAsync(user);
                System.out.println("success!");
                System.out.println();
            }else if(choice == 2){
                for(User user:users){
                    System.out.println(user);
                }
            }
        }


    }

}
