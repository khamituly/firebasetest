import com.google.firebase.database.*;

import java.io.IOException;
import java.net.URISyntaxException;

public class ShowDbChanges implements Runnable {
    public void run() {
        FireBaseService fbs = null;

        try {
            fbs = new FireBaseService();
        } catch (IOException | URISyntaxException e) {
            e.printStackTrace();
        }

        DatabaseReference ref = fbs.getDb()
                .getReference("/");
        ref.addValueEventListener(new ValueEventListener() {

            public void onDataChange(DataSnapshot dataSnapshot) {
                Object document = dataSnapshot.getValue();
                System.out.println(document);
            }


            public void onCancelled(DatabaseError error) {
                System.out.print("Error error.getMessage()"); }

        });


    }
}
